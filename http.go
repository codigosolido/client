package client

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

func (c *Client) HttpRequest(method, path string, body interface{}, response interface{}) error {
	// Get url
	rel, err := url.Parse(path)
	if err != nil {
		return err
	}
	url := c.BaseUrl.ResolveReference(rel)
	// Prepare data buffer
	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return err
		}
	}
	// Create Request
	req, err := http.NewRequest(method, url.String(), buf)
	if err != nil {
		return err
	}
	// Set headers
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)
	// Perform the request
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// Request error
	if resp.StatusCode >= http.StatusBadRequest {
		bytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return err
		}
		return errors.New(string(bytes))
	}
	// Decode response
	if response != nil {
		err = json.NewDecoder(resp.Body).Decode(response)
	}
	// Return
	return err
}
