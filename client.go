package client

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"

	guuid "github.com/google/uuid"
	"github.com/juandiego-cs/f3-classes"
)

type Client struct {
	BaseUrl        url.URL
	OrganisationId string
	UserAgent      string
	httpClient     http.Client
}

// Constructor
func NewClient(organisationId string, baseUrl string) (*Client, error) {
	// Check inputs
	if organisationId == "" {
		return nil, errors.New("empty organistaionId")
	}
	if baseUrl == "" {
		return nil, errors.New("empty API Url")
	}
	// Create Base Url
	base, err := url.Parse(baseUrl)
	if err != nil {
		return nil, err
	}
	// Invalid host
	if base.Host == "" {
		return nil, errors.New(("invalid host"))
	}
	c := new(Client)
	c.BaseUrl = *base
	c.OrganisationId = organisationId
	c.UserAgent = "API/Client"
	c.httpClient = http.Client{}
	return c, nil
}

// Create Account
func (c *Client) Create(account classes.Account) (*classes.Message, error) {
	//  Create the message
	message := classes.Message{
		Data: classes.Resource{
			Type:           "accounts",
			Id:             guuid.New().String(),
			OrganisationId: c.OrganisationId,
			Attributes:     account,
		},
	}
	// Http request
	response := new(classes.Message)
	err := c.HttpRequest("POST", "/v1/organisation/accounts", message, response)
	// Send response
	return response, err
}

// Fetch Account
func (c *Client) Fetch(id string) (*classes.Message, error) {
	// Http request
	response := new(classes.Message)
	err := c.HttpRequest("GET", "/v1/organisation/accounts/"+id, nil, response)
	// Send response
	return response, err
}

// Delete Account
func (c *Client) Delete(id string, version int) (*classes.Message, error) {
	// Http request
	response := new(classes.Message)
	err := c.HttpRequest("GET", "/v1/organisation/accounts/"+id, nil, response)
	if err != nil {
		return response, err
	}
	err = c.HttpRequest("DELETE", fmt.Sprintf("%s%s%s%d", "/v1/organisation/accounts/", id, "?version=", version), nil, nil)
	// Send response
	return response, err
}
