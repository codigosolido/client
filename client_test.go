package client_test

import (
	"net/url"
	"testing"

	"github.com/juandiego-cs/f3-client"
)

var clientId string = "634e3a41-26b8-49f9-a23d-26fa92061f38"
var baseUrl url.URL = url.URL{Scheme: "http", Host: "localhost:8080"}

func TestNewClient(t *testing.T) {
	type args struct {
		organisationId string
		baseUrl        string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{"good parameters", args{clientId, baseUrl.String()}, false},
		{"bad clientid", args{"", baseUrl.String()}, true},
		{"bad url", args{clientId, "invalidurl"}, true},
		{"null url", args{clientId, ""}, true},
		{"null clientid", args{"", baseUrl.String()}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := client.NewClient(tt.args.organisationId, tt.args.baseUrl)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewClient() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
